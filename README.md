# ohshitdocker

Helpful reference for docker commands

# Inspiration

Modeled after the ever helpful [ohshitgit.com](https://github.com/ksylor/ohshitgit)

# Tips

### How much space is docker using on my local system?

```
$ docker system df
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              15                  0                   2.675GB             2.675GB (100%)
Containers          0                   0                   0B                  0B
Local Volumes       10                  0                   132.8MB             132.8MB (100%)
Build Cache         0                   0                   0B                  0B
```

### How do I see all the containers that are running?

```
$ docker ps
TODO
```

### How do I see all the containers that exist, running or not?

```
$ docker ps -a
TODO
```

### How do I launch a quick container to give me a linux environment?

```
$ docker run -it --rm alpine
```

### How do I see the logs for a running container?

```
$ docker logs <container-ID>
```

### How do run a command on an already running container?

```
$ docker exec -it <container-ID> /bin/bash
```
